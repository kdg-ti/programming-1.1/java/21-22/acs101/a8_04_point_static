import draw.Point;

import java.util.MissingFormatArgumentException;
import java.util.MissingFormatWidthException;

public class PointDemo {
  public static void main(String[] args) {
    Point [] points = {new Point(1,2),new Point(12,45)};
    //Point.COLOR="blue";
    for (int i = 0; i < points.length; i++) {
      System.out.println(points[i]);
    }
    saySomething(points,"What's up, doc?");
    System.out.println("Exiting program...");
  }

  public static void  saySomething(Point[] points,String txt){
    try {
      System.out.printf("Distance is fixed");//, Point.distance(points[0], points[1]));
    }catch(Exception e){
      System.out.println("error message: "+e.getMessage());
      e.printStackTrace();
    }
//    catch(MissingFormatArgumentException e){
//      System.out.println("please supply an argument");
//    }
    System.out.println(txt);
  }
}
