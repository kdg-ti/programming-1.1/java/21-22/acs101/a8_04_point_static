package draw;

public class Point {
  int x,y;
  public final static String COLOR = "red";
  private static int count=0;
  private int myNumber;

  public Point(int x, int y) {
    this.x = x;
    this.y = y;
    System.out.println("Object number " + ++count + " has been created");
    myNumber=count;
  }

  public static double distance(Point first,Point second){
    return Math.sqrt(Math.pow(first.x-second.x,2) + Math.pow(first.y-second.y,2));
  }

  public  double distanceTo(Point second){
    return Math.sqrt(Math.pow(x-second.x,2) + Math.pow(y-second.y,2));
  }

  @Override
  public String toString() {
    return "Point{" +
      "x=" + x +
      ", y=" + y +
      ", color= " + Point.COLOR +
     "Object number " + myNumber+
      '}';
  }
}
